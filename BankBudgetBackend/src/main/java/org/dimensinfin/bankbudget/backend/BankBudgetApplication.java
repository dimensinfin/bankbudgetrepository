package org.dimensinfin.bankbudget.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankBudgetApplication {
	public static void main( String[] args ) {
		SpringApplication.run(BankBudgetApplication.class, args);
	}
}
