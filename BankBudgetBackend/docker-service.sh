#!/bin/bash
# - PARAMETERS & CONSTANTS
COMMAND=$1
ENVIRONMENT=$2

# - CONFIGURATION VARIABLES
source docker-env.sh

WORKING_DIRECTORY="$(dirname "$0")"
DOCKER_DIRECTORY="${WORKING_DIRECTORY}/src/main/resources/docker"
DOCKER_COMPOSER_COMMAND="docker-compose --file src/test/scripts/${PROJECT_NAME_PREFIX}-docker-${ENVIRONMENT}/docker-compose.yml"

if [ -z "$ENVIRONMENT" ]
then
  echo "Missing target-environment on command - { acceptance | integration }"
  exit 2
fi

# - G E N E R A T E   C O N T A I N E R
generateContainer() {
  cd "${WORKING_DIRECTORY}" || exit 1;
  rm -v "${DOCKER_DIRECTORY}"/*.jar
  ./gradlew clean bootJar
  cp ./build/libs/*.jar "$DOCKER_DIRECTORY"
  cp ./build/resources/main/app-banner.txt "$DOCKER_DIRECTORY"
  cp ./build/resources/main/app-banner.txt ./build/libs/app-banner.txt
  cd "$DOCKER_DIRECTORY" || exit 1;
  mv -v "${PROJECT_NAME_PREFIX}-backend-"*.jar "${PROJECT_NAME_PREFIX}-backend-container.jar"
  echo "${DOCKER_DIRECTORY}/Dockerfile"
  docker build -t ${CONTAINER_NAME} .
}
# - S T A R T / S T O P
start() {
  cd "${WORKING_DIRECTORY}" || exit 1;
  RUN_COMMAND="${DOCKER_COMPOSER_COMMAND} ${DOCKER_PREFERENCES}"
  $RUN_COMMAND up &
}
stop() {
  cd "${WORKING_DIRECTORY}" || exit 1;
  RUN_COMMAND="${DOCKER_COMPOSER_COMMAND}"
  $RUN_COMMAND down
}
recycle() {
  # - STOP
  cd "${WORKING_DIRECTORY}" || exit 1;
  RUN_COMMAND="${DOCKER_COMPOSER_COMMAND}"
  $RUN_COMMAND down
  generateContainer
  cd "${WORKING_DIRECTORY}" || exit 1;
  RUN_COMMAND="${DOCKER_COMPOSER_COMMAND}"
  $RUN_COMMAND up &
}

case $COMMAND in
'generate')
  generateContainer
  ;;
'start')
  start
  ;;
'stop')
  stop
  ;;
'recycle')
  recycle
  ;;
*)
  echo "Usage: $0 { generate | start | stop | recycle } target-environment"
  echo
  exit 1
  ;;
esac
exit 0
