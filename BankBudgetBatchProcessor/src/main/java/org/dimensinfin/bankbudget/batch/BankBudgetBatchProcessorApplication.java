package org.dimensinfin.bankbudget.batch;
import org.apache.commons.logging.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.dimensinfin.logging.LogWrapper;

@SpringBootApplication
public class BankBudgetBatchProcessorApplication {
	public static void main( String[] args ) {
//		LogWrapper.enter();
		SpringApplication.run(BankBudgetBatchProcessorApplication.class, args);
		LogWrapper.exit();
	}
}
